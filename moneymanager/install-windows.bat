@echo off
echo "############################################"
echo "install MoneyManager ...";
 
:XAMPP installieren:
echo "############################################"
echo "1.  install MySQL ..."
set /P quest=XAMPP is required. Do you want to install it? (y/n):
if "%quest%" == "y" start xampp-win32-5.5.24-0-VC11-installer.exe
if %errorlevel%==0 goto success
if %errorlevel%==1 goto error
 
:success
:Moneymanager in Programmordner verschieben:
echo "############################################"
echo "2. move MoneyManager to the Application folder"
cd ..
move moneymanager C:\Program Files
if %errorlevel%==0 goto finish
if %errorlevel%==1 goto error
 
:finish
:schließen:
echo "installation finished!"
set /P enter=Press any key...:
taskkill /F /IM cmd.exe
 
:error
echo "Installation not successfull. Please try again or contact Developer."
set /P enter=Press any key...: