#!/bin/sh
echo "############################################"
echo "install MoneyManager ...";
# MySQL installieren
echo "############################################"
echo "1.  install MySQL ..."
command -v mysql >/dev/null || ( echo "mysql is required but it's not installed" && read -p "Do you want to install mysql now ? [y/n]: " RESP
if [[ "$RESP" = "y" || "$RESP" = "yes" ]]; then
  echo "Installing.."
  hdiutil mount mysql-5.6.25-osx10.9-x86_64.dmg
  sudo cp -R "/Volumes/mysql-5.6.25-osx10.8-x86_64.app" /Applications
  sudo installer -package /usr/local/mysql/bin/mysql -target "/Volumes/Macintosh HD"
  cd ~
  hdiutil unmount "/Volumes/mysql-5.6.25-osx10.8-x86_64"
else
  echo "without mysql the command cannot be executed. Aborting."
  exit 0;
fi
)
type mysql >/dev/null 2>&1 && echo "MySQL present." || (
  echo "MySQL not present."
  exit 0;
)

echo "############################################"
echo "2. move MoneyManager to the Application folder"
cd ..
cp -R moneymanager /Applications
echo "installation finished!"
