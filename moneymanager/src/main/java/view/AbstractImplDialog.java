package view;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import model.DataAbstractImpl;

@SuppressWarnings({ "rawtypes", "serial" })
public abstract class AbstractImplDialog<T extends DataAbstractImpl> extends
		AbstractDialog {

	protected T actualImpl = null;

	public AbstractImplDialog(String name) {
		super(name);
	}

	public abstract boolean saveMaske();

	public T getCurrentImpl() {
		return actualImpl;

	}

	public abstract T storeValuesInImpl();

	public abstract void deleteImpl();

	protected FocusListener initFocusListener() {
		final AbstractImplDialog<?> abstractDialog = this;
		FocusListener focuslistener = new FocusListener() {

			@Override
			public void focusLost(FocusEvent arg0) {
				MainDialog mainDialog = (MainDialog) SwingUtilities
						.getWindowAncestor(abstractDialog);
				mainDialog.enableSave();
			}

			@Override
			public void focusGained(FocusEvent arg0) {

			}
		};
		return focuslistener;
	}

	@Override
	public void registerEvents() {
		super.registerEvents();
	}

	public abstract void updateView();

	public abstract void enableMaske(boolean bearb);

	@Override
	public void closeThis(boolean save) {
		if (save) {
			if (actualImpl.isEqual(storeValuesInImpl())) {
				super.closeThis(save);
			} else {
				int result = JOptionPane.showConfirmDialog(this, "Speichern",
						"Möchten sie speichern ? ", JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					saveMaske();
					super.closeThis(false);
				} else {
					super.closeThis(false);
				}
			}
		}else {
			super.closeThis(false);
		}

	}

	@Override
	public String[] getEnableButtons() {
		return new String[] { "Edit", "Webcam", "ImportDatei", "Hilfe", "Close", "Excel", "Numbers" };
	}

}
