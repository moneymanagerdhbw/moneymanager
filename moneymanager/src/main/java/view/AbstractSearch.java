package view;

import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

public abstract class AbstractSearch  extends JPanel {
	private JLabel lblSuche;
	private JLabel lblSuche_1;
	private JLabel lblSuche_2;
	private JTextField txtSuche1;
	private JTextField txtSuche2;
	private JTextField txtSuche3;
	
	
	
	public AbstractSearch(String suche1, String suche2, String suche3) {
		if(suche1 != null) {
			getLblSuche().setText(suche1);
		}else {
			getLblSuche().setVisible(false);
			getTxtSuche1().setVisible(false);
		}
		if(suche2 != null) {
			getLblSuche_1().setText(suche2);
		}else {
			getLblSuche_1().setVisible(false);
			getTxtSuche2().setVisible(false);
		}
		if(suche3 != null) {
			getLblSuche_2().setText(suche3);
		}else {
			getLblSuche_2().setVisible(false);
			getTxtSuche3().setVisible(false);
		}
		initialize();
	}

	
	
	private void initialize() {
		setLayout(new MigLayout("", "[][grow]", "[][][]"));
		add(getLblSuche(), "cell 0 0,alignx trailing");
		add(getTxtSuche1(), "cell 1 0,growx,aligny top");
		add(getLblSuche_1(), "cell 0 1,alignx trailing");
		add(getTxtSuche2(), "cell 1 1,growx");
		add(getLblSuche_2(), "cell 0 2,alignx trailing");
		add(getTxtSuche3(), "cell 1 2,growx");
	}
	
	
	
	public abstract String getSearchSQL();
	
	public abstract DefaultTableModel getTableModel();
	
	
	private JLabel getLblSuche() {
		if (lblSuche == null) {
			lblSuche = new JLabel("Suche1");
		}
		return lblSuche;
	}
	private JLabel getLblSuche_1() {
		if (lblSuche_1 == null) {
			lblSuche_1 = new JLabel("Suche2");
		}
		return lblSuche_1;
	}
	private JLabel getLblSuche_2() {
		if (lblSuche_2 == null) {
			lblSuche_2 = new JLabel("Suche3");
		}
		return lblSuche_2;
	}
	protected JTextField getTxtSuche1() {
		if (txtSuche1 == null) {
			txtSuche1 = new JTextField();
			txtSuche1.setColumns(10);
		}
		return txtSuche1;
	}
	protected JTextField getTxtSuche2() {
		if (txtSuche2 == null) {
			txtSuche2 = new JTextField();
			txtSuche2.setColumns(10);
		}
		return txtSuche2;
	}
	protected JTextField getTxtSuche3() {
		if (txtSuche3 == null) {
			txtSuche3 = new JTextField();
			txtSuche3.setColumns(10);
		}
		return txtSuche3;
	}
}
