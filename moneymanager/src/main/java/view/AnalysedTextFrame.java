package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;

import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

public class AnalysedTextFrame extends JFrame{
	private JPanel pPanel;
	private JTextArea textArea;
	
	public AnalysedTextFrame(String text) {
		super("Analysierter Text");
		getContentPane().add(getPPanel(), BorderLayout.CENTER);
		textArea.setText(text);
		
		setSize(500, 500);
		setLocation(200, 200);
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private JPanel getPPanel() {
		if (pPanel == null) {
			pPanel = new JPanel();
			pPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
			pPanel.setLayout(new BorderLayout(0, 0));
			pPanel.add(new JScrollPane(getTextArea()), BorderLayout.CENTER);
		}
		return pPanel;
	}
	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
		}
		return textArea;
	}
}
