package view;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.script.SimpleScriptContext;
import javax.swing.JFrame;

import model.DataAbstractImpl;
import model.DatabaseException;
import model.category.CategoryService;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import net.miginfocom.swing.MigLayout;

import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.border.EmptyBorder;

import java.awt.SystemColor;

import javax.swing.JLabel;

import com.ibatis.common.jdbc.ScriptRunner;
import com.mysql.jdbc.Connection;
import com.sun.jna.platform.unix.X11.XClientMessageEvent.Data;

import java.awt.CardLayout;
import java.awt.Color;

public class MainDialog extends JFrame {

	private List<AbstractDialog> dialogList;
	private JPanel pBackground;
	private JMenuBar menuBar;
	private JMenu mnDatei;
	private JMenu mnAnsicht;
	private JMenu mnNavigation;
	private JMenu mnExport;
	private JMenu mnImport;
	private JMenu mnHilfe;
	private JMenuItem mntmNew;
	private JMenuItem mntmEdit;
	private JMenuItem mntmSave;
	private JMenuItem mntmDelete;
	private JMenuItem mntmClose;
	private JCheckBoxMenuItem chckbxmntmDateimen;
	private JCheckBoxMenuItem chckbxmntmNavigationsmen;
	private JCheckBoxMenuItem chckbxmntmExportmen;
	private JCheckBoxMenuItem chckbxmntmImportmen;
	private JCheckBoxMenuItem chckbxmntmHilfe;
	private JMenuItem mntmUp;
	private JMenuItem mntmDown;
	private JMenuItem mntmExcel;
	private JMenuItem mntmNumbers;
	private JMenuItem mntmWebcam;
	private JMenuItem mntmImportDatei;
	private JMenuItem mntmHilfe;
	private JPanel pMenubarBackground;
	private JToolBar tbDatei;
	private JButton btnSave;
	private JMenu mnSuche;
	private JMenuItem mntmSearch;
	private JButton btnNew;
	private JButton btnEdit;
	private JButton btnDelete;
	private JToolBar tbSuche;
	private JButton btnSearch;
	private JToolBar tbNavigate;
	private JButton btnUp;
	private JButton btnDown;
	private JToolBar tbExport;
	private JButton btnExcel;
	private JButton btnNumbers;
	private JToolBar tbImport;
	private JButton btnImportDatei;
	private JButton btnWebcam;
	private JToolBar tbHilfe;
	private JButton btnHilfe;
	private JPanel pDialogBackground;
	private JToolBar tbStatus;
	private JLabel lblStatus;

	public MainDialog() {
		initialize();
		setVisible(true);
		setSize(1000, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocation(100, 100);
		dialogList = new ArrayList<AbstractDialog>();
		enableButtons();
	}

	private void initialize() {
		getContentPane().add(getPBackground(), BorderLayout.CENTER);
		setJMenuBar(getMenuBar_1());
		registerEvents();
	}

	public void enableDialog(boolean enable) {
		this.setEnabled(enable);
	}
	
	private void registerEvents() {
		
		btnImportDatei.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				addImplDialog(new DateiEinlesenDialog());
			}
		});
		
		btnSearch.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				try {
					addImplDialog(new SucheDialog());
				} catch (DatabaseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnWebcam.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				addImplDialog(new WebcamDialog());

			}
		});

		btnNew.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				AbstractDialog dialog = dialogList.get(dialogList.size() - 1);
				if (dialog instanceof SucheDialog) {
					SucheDialog suche = (SucheDialog) dialog;
					String selectedItem = (String) suche.getComboBox()
							.getSelectedItem();
					try {
						switch (selectedItem) {
						case "Kategorie":
							addImplDialog(new CategoryDialog(-1));
							break;
						case "Einzahlung":
							addImplDialog(new IncomingDialog(-1));
							break;
						case "Auszahlung":
							addImplDialog(new OutgoingDialog(-1));
							break;
						}
					} catch (DatabaseException e) {
						JOptionPane.showMessageDialog(null,
								"Fehler in der Datenbank");
					}

				}
			}
		});

		btnEdit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				AbstractDialog dialog = dialogList.get(dialogList.size() - 1);
				if (dialog instanceof AbstractImplDialog<?>) {
					AbstractImplDialog<?> implDialog = (AbstractImplDialog<?>) dialog;
					implDialog.enableMaske(true);
					DataAbstractImpl<?> abstractImpl = implDialog
							.getCurrentImpl();
					if (abstractImpl.getSurkey() != -1)
						btnDelete.setEnabled(true);
					btnEdit.setEnabled(false);
				}
			}
		});
		
		btnSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AbstractImplDialog<?> implDialog = (AbstractImplDialog<?>) dialogList.get(dialogList.size()-1);
				implDialog.saveMaske();
			}
		});
		
		btnDelete.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AbstractImplDialog<?> implDialog = (AbstractImplDialog<?>) dialogList.get(dialogList.size()-1);
				implDialog.deleteImpl();
				
			}
		});
		
		btnUp.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AbstractDialog dialog = dialogList.get(0);
				dialogList.remove(dialog);
				dialogList.add(dialog);
				addPanelToMainDialog();
			}
		});
		
		btnDown.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				AbstractDialog dialog = dialogList.get(dialogList.size()-2);
				dialogList.remove(dialog);
				dialogList.add(dialog);
				addPanelToMainDialog();
			}
		});
		
		btnExcel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addImplDialog(new ExportDialog("Excel"));
			}
		});
		
		btnNumbers.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addImplDialog(new ExportDialog("Numbers"));
				
			}
		});
	}

	public void addImplDialog(AbstractDialog dialog) {
		pDialogBackground.removeAll();
		AbstractDialog tempDialog = null;
		for (AbstractDialog oDialog : dialogList) {
			if (oDialog.getClass().equals(dialog.getClass())) {
				tempDialog = dialog;
			}
		}

		if (tempDialog != null) {
			dialogList.remove(tempDialog);
			dialogList.add(tempDialog);
		} else {
			dialogList.add(dialog);
		}

				
		dialog.setVisible(true);
		addPanelToMainDialog();
		
	}
	
	private void enableUpDown(boolean enable) {
		btnUp.setEnabled(enable);
		btnDown.setEnabled(enable);
	}

	
	private void addPanelToMainDialog() {
		for (int i = dialogList.size() - 1; i >= 0; i--) {
			pDialogBackground.add(dialogList.get(i));
		}

		dialogList.get(0).updateView();
		pDialogBackground.updateUI();
		
		enableButtons();
		if(dialogList.size() > 1) {
			enableUpDown(true);
		}
	}

	public void closeAbstractDialog(AbstractDialog dialog) {
		dialog.setVisible(false);

		pDialogBackground.remove(dialog);
		dialogList.remove(dialog);

		for (AbstractDialog mDialog : dialogList) {
			mDialog.updateView();
		}
		enableButtons();
		if(dialogList.size() > 1) {
			enableUpDown(true);
		}
	}

	public void enableSave() {
		btnSave.setEnabled(true);
		mntmSave.setEnabled(true);
	}

	private void enableButtons() {
		if (!dialogList.isEmpty()) {
			Class<?> class1 = this.getClass();
			Field[] fields = MainDialog.class.getDeclaredFields();
			for (Field field : fields) {
				Object o;
				try {
					o = field.get(this);
					if ((o instanceof JButton || o instanceof JMenuItem)
							&& !(o instanceof JMenu)
							&& !(o instanceof JCheckBoxMenuItem)) {
						AbstractDialog mDialog = dialogList.get(dialogList
								.size() - 1);
						String[] toEnablingButtons = mDialog.getEnableButtons();
						JComponent c = (JComponent) o;
						c.setEnabled(false);
						for (String button : toEnablingButtons) {
							if (field.getName().indexOf(button) != -1) {
								c.setEnabled(true);
							}
						}
					}
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}

		} else {
			btnDelete.setEnabled(false);
			btnDown.setEnabled(false);
			btnEdit.setEnabled(false);
			btnExcel.setEnabled(true);
			btnHilfe.setEnabled(true);
			btnImportDatei.setEnabled(true);
			btnNew.setEnabled(false);
			btnNumbers.setEnabled(true);
			btnSearch.setEnabled(true);
			btnSave.setEnabled(false);
			btnUp.setEnabled(false);
			btnWebcam.setEnabled(true);

			mntmDelete.setEnabled(false);
			mntmDown.setEnabled(false);
			mntmEdit.setEnabled(false);
			mntmExcel.setEnabled(false);
			mntmHilfe.setEnabled(true);
			mntmImportDatei.setEnabled(true);
			mntmNew.setEnabled(false);
			mntmNumbers.setEnabled(false);
			mntmSearch.setEnabled(true);
			mntmSave.setEnabled(false);
			mntmUp.setEnabled(false);
			mntmWebcam.setEnabled(true);
		}
	}

	public static void main(String[] args) {
		try {
			
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			}
			
			CategoryService service = new CategoryService();
			if(!service.hasConnection()) {
				JOptionPane.showMessageDialog(null, "Datenbank konnte nicht geladen werden!\n\r Server starten!"); 
				
				String path = "sql-scripts/db_username_template.sql";
				
				
				Class.forName("com.mysql.jdbc.Driver");
				java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost/", "root", "");
				ScriptRunner runner = new ScriptRunner(con, false, false);
				Reader reader = new BufferedReader(new FileReader(path));
				runner.runScript(reader);
			}else {
				MainDialog dialog = new MainDialog();
			}
		
		} catch (DatabaseException e) {
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private JPanel getPBackground() {
		if (pBackground == null) {
			pBackground = new JPanel();
			pBackground.setLayout(new BorderLayout(0, 0));
			pBackground.add(getPanel_1(), BorderLayout.NORTH);
			pBackground.add(getPDialogBackground(), BorderLayout.CENTER);
			pBackground.add(getToolBar_5(), BorderLayout.SOUTH);
		}
		return pBackground;
	}

	private JMenuBar getMenuBar_1() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getMnDatei());
			menuBar.add(getMnSuche());
			menuBar.add(getMnAnsicht());
			menuBar.add(getMnNavigation());
			menuBar.add(getMnExport());
			menuBar.add(getMnImport());
			menuBar.add(getMnHilfe());
		}
		return menuBar;
	}

	private JMenu getMnDatei() {
		if (mnDatei == null) {
			mnDatei = new JMenu("Datei");
			mnDatei.add(getMntmNew());
			mnDatei.add(getMntmEdit());
			mnDatei.add(getMntmSave());
			mnDatei.add(getMntmDelete());
			mnDatei.add(getMntmClose());
		}
		return mnDatei;
	}

	private JMenu getMnAnsicht() {
		if (mnAnsicht == null) {
			mnAnsicht = new JMenu("Ansicht");
			mnAnsicht.add(getChckbxmntmDateimen());
			mnAnsicht.add(getChckbxmntmNavigationsmen());
			mnAnsicht.add(getChckbxmntmExportmen());
			mnAnsicht.add(getChckbxmntmImportmen());
			mnAnsicht.add(getChckbxmntmHilfe());
		}
		return mnAnsicht;
	}

	private JMenu getMnNavigation() {
		if (mnNavigation == null) {
			mnNavigation = new JMenu("Navigation");
			mnNavigation.add(getMntmUp());
			mnNavigation.add(getMntmDown());
		}
		return mnNavigation;
	}

	private JMenu getMnExport() {
		if (mnExport == null) {
			mnExport = new JMenu("Export");
			mnExport.add(getMntmExcel());
			mnExport.add(getMntmNumbers());
		}
		return mnExport;
	}

	private JMenu getMnImport() {
		if (mnImport == null) {
			mnImport = new JMenu("Import");
			mnImport.add(getMntmWebcam());
			mnImport.add(getMntmImportDatei());
		}
		return mnImport;
	}

	private JMenu getMnHilfe() {
		if (mnHilfe == null) {
			mnHilfe = new JMenu("Hilfe");
			mnHilfe.add(getMntmHilfe());
		}
		return mnHilfe;
	}

	private JMenuItem getMntmNew() {
		if (mntmNew == null) {
			mntmNew = new JMenuItem("Neu");
		}
		return mntmNew;
	}

	private JMenuItem getMntmEdit() {
		if (mntmEdit == null) {
			mntmEdit = new JMenuItem("Bearbeiten");
		}
		return mntmEdit;
	}

	private JMenuItem getMntmSave() {
		if (mntmSave == null) {
			mntmSave = new JMenuItem("Speichern");
		}
		return mntmSave;
	}

	private JMenuItem getMntmDelete() {
		if (mntmDelete == null) {
			mntmDelete = new JMenuItem("Löschen");
		}
		return mntmDelete;
	}

	private JMenuItem getMntmClose() {
		if (mntmClose == null) {
			mntmClose = new JMenuItem("Schließen");
		}
		return mntmClose;
	}

	private JCheckBoxMenuItem getChckbxmntmDateimen() {
		if (chckbxmntmDateimen == null) {
			chckbxmntmDateimen = new JCheckBoxMenuItem("Dateimenü");
		}
		return chckbxmntmDateimen;
	}

	private JCheckBoxMenuItem getChckbxmntmNavigationsmen() {
		if (chckbxmntmNavigationsmen == null) {
			chckbxmntmNavigationsmen = new JCheckBoxMenuItem("Navigationsmenü");
		}
		return chckbxmntmNavigationsmen;
	}

	private JCheckBoxMenuItem getChckbxmntmExportmen() {
		if (chckbxmntmExportmen == null) {
			chckbxmntmExportmen = new JCheckBoxMenuItem("Exportmenü");
		}
		return chckbxmntmExportmen;
	}

	private JCheckBoxMenuItem getChckbxmntmImportmen() {
		if (chckbxmntmImportmen == null) {
			chckbxmntmImportmen = new JCheckBoxMenuItem("Importmenü");
		}
		return chckbxmntmImportmen;
	}

	private JCheckBoxMenuItem getChckbxmntmHilfe() {
		if (chckbxmntmHilfe == null) {
			chckbxmntmHilfe = new JCheckBoxMenuItem("Hilfe");
		}
		return chckbxmntmHilfe;
	}

	private JMenuItem getMntmUp() {
		if (mntmUp == null) {
			mntmUp = new JMenuItem("Vor...");
		}
		return mntmUp;
	}

	private JMenuItem getMntmDown() {
		if (mntmDown == null) {
			mntmDown = new JMenuItem("Zurück...");
		}
		return mntmDown;
	}

	private JMenuItem getMntmExcel() {
		if (mntmExcel == null) {
			mntmExcel = new JMenuItem("Export in Excel");
		}
		return mntmExcel;
	}

	private JMenuItem getMntmNumbers() {
		if (mntmNumbers == null) {
			mntmNumbers = new JMenuItem("Export in Numbers");
		}
		return mntmNumbers;
	}

	private JMenuItem getMntmWebcam() {
		if (mntmWebcam == null) {
			mntmWebcam = new JMenuItem("Webcam");
		}
		return mntmWebcam;
	}

	private JMenuItem getMntmImportDatei() {
		if (mntmImportDatei == null) {
			mntmImportDatei = new JMenuItem("Datei");
		}
		return mntmImportDatei;
	}

	private JMenuItem getMntmHilfe() {
		if (mntmHilfe == null) {
			mntmHilfe = new JMenuItem("Hilfe");
		}
		return mntmHilfe;
	}

	private JPanel getPanel_1() {
		if (pMenubarBackground == null) {
			pMenubarBackground = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pMenubarBackground.getLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			pMenubarBackground.add(getTbDatei());
			pMenubarBackground.add(getTbSuche());
			pMenubarBackground.add(getToolBar_1());
			pMenubarBackground.add(getToolBar_2());
			pMenubarBackground.add(getToolBar_3());
			pMenubarBackground.add(getToolBar_4());
		}
		return pMenubarBackground;
	}

	private JToolBar getTbDatei() {
		if (tbDatei == null) {
			tbDatei = new JToolBar();
			tbDatei.add(getBtnNew());
			tbDatei.add(getBtnEdit());
			tbDatei.add(getBtnSave());
			tbDatei.add(getBtnDelete());
		}
		return tbDatei;
	}

	private JButton getBtnSave() {
		if (btnSave == null) {
			btnSave = new JButton("\r\n");
			btnSave.setIcon(new ImageIcon(
					"images/save.png"));
		}
		return btnSave;
	}

	private JMenu getMnSuche() {
		if (mnSuche == null) {
			mnSuche = new JMenu("Suche");
			mnSuche.add(getMntmSearch());
		}
		return mnSuche;
	}

	private JMenuItem getMntmSearch() {
		if (mntmSearch == null) {
			mntmSearch = new JMenuItem("Stammdaten Suche");
		}
		return mntmSearch;
	}

	private JButton getBtnNew() {
		if (btnNew == null) {
			btnNew = new JButton("");
			btnNew.setIcon(new ImageIcon("images/new.png"));
		}
		return btnNew;
	}

	private JButton getBtnEdit() {
		if (btnEdit == null) {
			btnEdit = new JButton("");
			btnEdit.setIcon(new ImageIcon("images/edit.png"));
		}
		return btnEdit;
	}

	private JButton getBtnDelete() {
		if (btnDelete == null) {
			btnDelete = new JButton("");
			btnDelete.setIcon(new ImageIcon("images/delete.png"));
		}
		return btnDelete;
	}

	private JToolBar getTbSuche() {
		if (tbSuche == null) {
			tbSuche = new JToolBar();
			tbSuche.add(getBtnSearch());
		}
		return tbSuche;
	}

	private JButton getBtnSearch() {
		if (btnSearch == null) {
			btnSearch = new JButton("");
			btnSearch.setIcon(new ImageIcon("images/search.png"));
		}
		return btnSearch;
	}

	private JToolBar getToolBar_1() {
		if (tbNavigate == null) {
			tbNavigate = new JToolBar();
			tbNavigate.add(getBtnUp());
			tbNavigate.add(getBtnDown());
		}
		return tbNavigate;
	}

	private JButton getBtnUp() {
		if (btnUp == null) {
			btnUp = new JButton("");
			btnUp.setIcon(new ImageIcon("images/up.png"));
		}
		return btnUp;
	}

	private JButton getBtnDown() {
		if (btnDown == null) {
			btnDown = new JButton("");
			btnDown.setIcon(new ImageIcon("images/down.png"));
		}
		return btnDown;
	}

	private JToolBar getToolBar_2() {
		if (tbExport == null) {
			tbExport = new JToolBar();
			tbExport.add(getBtnExcel());
			tbExport.add(getBtnNumbers());
		}
		return tbExport;
	}

	private JButton getBtnExcel() {
		if (btnExcel == null) {
			btnExcel = new JButton("");
			btnExcel.setIcon(new ImageIcon("images/excel.png"));
		}
		return btnExcel;
	}

	private JButton getBtnNumbers() {
		if (btnNumbers == null) {
			btnNumbers = new JButton("");
			btnNumbers.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				}
			});
			btnNumbers.setIcon(new ImageIcon("images/numbers.jpg"));
		}
		return btnNumbers;
	}

	private JToolBar getToolBar_3() {
		if (tbImport == null) {
			tbImport = new JToolBar();
			tbImport.add(getBtnImportDatei());
			tbImport.add(getBtnWebcam());
		}
		return tbImport;
	}

	private JButton getBtnImportDatei() {
		if (btnImportDatei == null) {
			btnImportDatei = new JButton("");
			btnImportDatei.setIcon(new ImageIcon("images/import.png"));
		}
		return btnImportDatei;
	}

	private JButton getBtnWebcam() {
		if (btnWebcam == null) {
			btnWebcam = new JButton("");
			btnWebcam.setIcon(new ImageIcon("images/webcam.png"));
		}
		return btnWebcam;
	}

	private JToolBar getToolBar_4() {
		if (tbHilfe == null) {
			tbHilfe = new JToolBar();
			tbHilfe.add(getBtnHilfe());
		}
		return tbHilfe;
	}

	private JButton getBtnHilfe() {
		if (btnHilfe == null) {
			btnHilfe = new JButton("");
			btnHilfe.setIcon(new ImageIcon("images/help.png"));
		}
		return btnHilfe;
	}

	private JPanel getPDialogBackground() {
		if (pDialogBackground == null) {
			pDialogBackground = new JPanel();
			pDialogBackground.setBackground(Color.LIGHT_GRAY);
			pDialogBackground.setBorder(new EmptyBorder(10, 10, 10, 11));
			pDialogBackground.setLayout(new CardLayout(0, 0));
		}
		return pDialogBackground;
	}

	private JToolBar getToolBar_5() {
		if (tbStatus == null) {
			tbStatus = new JToolBar();
			tbStatus.setFloatable(false);
			tbStatus.add(getLblStatus());
		}
		return tbStatus;
	}

	private JLabel getLblStatus() {
		if (lblStatus == null) {
			lblStatus = new JLabel("Status: ");
		}
		return lblStatus;
	}

}