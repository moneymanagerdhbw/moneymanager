package view;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import net.miginfocom.swing.MigLayout;
import net.sourceforge.tess4j.TesseractException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import controller.webcam.WebcamCapture;

public class DateiEinlesenDialog extends AbstractDialog {
	private JPanel pBackground;
	private JButton btnDateiAuswhlen;
	private JButton btnAnalysieren;
	private JLabel lblImage;
	private BufferedImage mImage;

	public DateiEinlesenDialog() {
		super("Datei einlesen");
		add(getPBackground(), BorderLayout.CENTER);
		registerEvents();
		btnAnalysieren.setEnabled(false);
	}
	
	
	public void registerEvents () {
		super.registerEvents();
		btnDateiAuswhlen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FileFilter filter = new FileNameExtensionFilter("Bilder", "png", "jpg");
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(filter);
				int result = chooser.showOpenDialog(null);
				if(result == JFileChooser.APPROVE_OPTION) {
					File file = chooser.getSelectedFile();
					ImageIcon icon = new ImageIcon(file.getAbsolutePath());
					lblImage.setIcon(icon);
					try {
						mImage = ImageIO.read(file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					btnAnalysieren.setEnabled(true);
				}
			}
		});
		btnAnalysieren.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					String result = WebcamCapture.ocrImagefile(mImage);
					new AnalysedTextFrame(result);
	
				} catch (TesseractException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		
	}

	@Override
	public String[] getEnableButtons() {
		// TODO Auto-generated method stub
		return new String []{"Help"};
	}

	@Override
	public void updateView() {
		// TODO Auto-generated method stub
		
	}

	private JPanel getPBackground() {
		if (pBackground == null) {
			pBackground = new JPanel();
			pBackground.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][][]"));
			pBackground.add(getBtnDateiAuswhlen(), "cell 0 0");
			pBackground.add(getLblImage(), "cell 0 1 2 7");
			pBackground.add(getBtnAnalysieren(), "cell 0 8");
		}
		return pBackground;
	}
	private JButton getBtnDateiAuswhlen() {
		if (btnDateiAuswhlen == null) {
			btnDateiAuswhlen = new JButton("Datei Auswählen");
		}
		return btnDateiAuswhlen;
	}
	private JButton getBtnAnalysieren() {
		if (btnAnalysieren == null) {
			btnAnalysieren = new JButton("Analysieren");
		}
		return btnAnalysieren;
	}
	private JLabel getLblImage() {
		if (lblImage == null) {
			lblImage = new JLabel("");
		}
		return lblImage;
	}
}
