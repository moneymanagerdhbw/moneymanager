package view;

import javax.naming.directory.SearchControls;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JComboBox;

import model.DataAbstractImpl;
import model.DatabaseException;
import model.DatabaseService;
import model.category.CategoryService;
import model.incoming.IncomingService;
import model.outgoing.OutgoingService;
import net.miginfocom.swing.MigLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class SucheDialog extends AbstractDialog {
	private JPanel pBackground;
	private JPanel pControl;
	private JComboBox comboBox;
	private JButton btnSuche;
	private JPanel pTable;
	private JLabel lblErgebnis;
	private JTable table;

	private CategoryService categoryService;
	private IncomingService incomingService;
	private OutgoingService outgoingService;

	private AbstractSearch searchPanel;

	private ArrayList<DataAbstractImpl<?>> resultList;

	public SucheDialog() throws DatabaseException {
		super("Stammdaten Suche");
		add(getPBackground(), BorderLayout.CENTER);
		registerEvents();
		searchPanel = SearchDialogs.getInstance(SearchDialogs.SEARCH_INCOMING);
		pControl.add(searchPanel, "cell 1 2,growx");
		categoryService = new CategoryService();
		incomingService = new IncomingService();
		outgoingService = new OutgoingService();
		table.setModel(searchPanel.getTableModel());
		resultList = new ArrayList<DataAbstractImpl<?>>();
	}

	@Override
	public String[] getEnableButtons() {
		return new String[] { "Hilfe", "New", "Close", "Webcam", "ImportDatei", "Excel", "Numbers" };
	}

	@Override
	public void updateView() {
		search();
	}

	@Override
	public void registerEvents() {
		super.registerEvents();

		comboBox.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent arg0) {
				if (comboBox.getSelectedItem() != null) {
					if (searchPanel != null)
						pControl.remove(searchPanel);
					String itemString = (String) comboBox.getSelectedItem();
					switch (itemString) {
					case "Einzahlung":
						searchPanel = SearchDialogs
								.getInstance(SearchDialogs.SEARCH_INCOMING);
						break;
					case "Auszahlung":
						searchPanel = SearchDialogs
								.getInstance(SearchDialogs.SEARCH_OUTGOING);
						break;
					case "Kategorie":
						searchPanel = SearchDialogs
								.getInstance(SearchDialogs.SEARCH_CATEGORY);
						break;
					}
					pControl.add(searchPanel, "cell 1 2,growx");
					table.setModel(searchPanel.getTableModel());
					pControl.updateUI();
				}

			}
		});
		btnSuche.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				search();

			}

		});

		final SucheDialog sucheDialog = this;

		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					if (table.getSelectedRow() != -1) {
						String itemString = (String) comboBox.getSelectedItem();
						MainDialog dialog = (MainDialog) SwingUtilities
								.getWindowAncestor(sucheDialog);

						try {
							switch (itemString) {
							case "Einzahlung":
								dialog.addImplDialog(new IncomingDialog(
										resultList.get(table.getSelectedRow())
												.getSurkey()));
								break;

							case "Auszahlung":
								dialog.addImplDialog(new OutgoingDialog(
										resultList.get(table.getSelectedRow())
												.getSurkey()));
								break;
							case "Kategorie":
								dialog = (MainDialog) SwingUtilities
										.getWindowAncestor(sucheDialog);
								dialog.addImplDialog(new CategoryDialog(
										resultList.get(table.getSelectedRow())
												.getSurkey()));
								break;
							}
						} catch (DatabaseException e1) {
							e1.printStackTrace();
						}

					}
				}

			}
		});

	}

	private void search() {
		final DefaultTableModel model = searchPanel.getTableModel();
		table.setModel(model);
		String itemString = (String) comboBox.getSelectedItem();
		try {
			switch (itemString) {
			case "Einzahlung":
				initTable(model, incomingService);
				break;
			case "Auszahlung":
				initTable(model, outgoingService);
				break;
			case "Kategorie":
				initTable(model, categoryService);
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Die Suche ist fehlgeschlagen");
		}
	}

	private void initTable(DefaultTableModel model, DatabaseService<?> service)
			throws SQLException {
		resultList.clear();
		ResultSet set = service.find(searchPanel.getSearchSQL());
		if (set != null) {
			while (set.next()) {
				DataAbstractImpl<?> impl = service.fillImpl(set);
				model.addRow(impl.getSearchValue());
				resultList.add(impl);
			}
		}

	}

	private JPanel getPBackground() {
		if (pBackground == null) {
			pBackground = new JPanel();
			pBackground.setLayout(new BorderLayout(0, 0));
			pBackground.add(getPControl(), BorderLayout.NORTH);
			pBackground.add(getPTable(), BorderLayout.CENTER);
		}
		return pBackground;
	}

	private JPanel getPControl() {
		if (pControl == null) {
			pControl = new JPanel();
			pControl.setLayout(new MigLayout("", "[200px][grow][][][][][][][]",
					"[20px][][]"));
			pControl.add(getComboBox(), "cell 0 0,growx");
			pControl.add(getBtnSuche(), "cell 8 2");
		}
		return pControl;
	}

	public JComboBox getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox();
			comboBox.addItem("Einzahlung");
			comboBox.addItem("Auszahlung");
			comboBox.addItem("Kategorie");
		}
		return comboBox;
	}

	private JButton getBtnSuche() {
		if (btnSuche == null) {
			btnSuche = new JButton("Suche");
		}
		return btnSuche;
	}

	private JPanel getPTable() {
		if (pTable == null) {
			pTable = new JPanel();
			pTable.setBorder(new EmptyBorder(5, 5, 5, 5));
			pTable.setLayout(new BorderLayout(0, 0));
			pTable.add(getLblErgebnis(), BorderLayout.NORTH);
			pTable.add(new JScrollPane(getTable()), BorderLayout.CENTER);
		}
		return pTable;
	}

	private JLabel getLblErgebnis() {
		if (lblErgebnis == null) {
			lblErgebnis = new JLabel("Ergebnis:");
		}
		return lblErgebnis;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
		}
		return table;
	}
}
