package view;

import model.DatabaseException;
import model.category.CategoryImpl;
import model.category.CategoryService;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;

import controller.excelexport.ExcelExport;
import controller.numbersexport.ExportToNumbers;

public class ExportDialog extends AbstractDialog {

	private String name = "";

	private CategoryService categoryService;
	private JPanel panel;
	private JLabel lblKategorie;
	private JLabel lblVon;
	private JLabel lblBis;
	private JTextField txtVon;
	private JTextField txtBis;
	private JTable table;
	private JButton btnExport;

	private ArrayList<Object[]> exportArray = new ArrayList<Object[]>();
	private final Object[] SPALTEN = new Object[] { "Datum", "Beschreibung",
			"Betrag", "Kategorie" };
	private JButton btnSuche;
	private JComboBox<CategoryImpl> comboBox;

	public ExportDialog(String name) {
		super("Export nach " + name);
		this.name = name;
		add(getPanel(), BorderLayout.CENTER);
		try {
			categoryService = new CategoryService();
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		initTableModel(new Object[][] {});
		registerEvents();
		updateCategory();

	}

	private void updateCategory() {
		ResultSet resultSet = categoryService.find("Select * from category");
		CategoryImpl actualCatImpl = null;
		comboBox.removeAllItems();
		comboBox.addItem(new CategoryImpl());
		
		if (resultSet != null) {
			try {
				while (resultSet.next()) {
					CategoryImpl mImpl = categoryService.fillImpl(resultSet);
					comboBox.addItem(mImpl);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void registerEvents() {
		super.registerEvents();
		btnExport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (name.equals("Excel")) {
					exportInExcel();
				} else {
					exportInNumbers();
				}
			}
		});

		btnSuche.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				search();

			}
		});

	}

	private void search() {
		exportArray.clear();
		String sql = "select a.*, c.name from (select date, description, value, "
				+ "category from incoming i union select date, description, value, "
				+ "category from outgoing o) a left outer join category c on (a.category = c.id)";
		String orderby = " order by a.date";
		String where = "";
		String cat = "";
		if (((CategoryImpl) comboBox.getSelectedItem()).getSurkey() != -1) {
			cat = " c.id = "
					+ ((CategoryImpl) comboBox.getSelectedItem()).getSurkey();
		}

		where += cat;
		if(!where.equals("")) where += " and ";
		
		where += txtVon.getText().equals("") ? "" : ("  a.date >= to_date('"
				+ txtVon.getText() + "','dd.MM.yyyy') ");
		where += txtBis.getText().equals("") ? "" : (" and a.date <= to_date('"
				+ txtBis.getText() + "','dd.MM.yyyy') ");
		sql += (where.equals("") ? "" : (" where " + where)) + orderby;

		ResultSet resultSet = categoryService.find(sql);
		if (resultSet != null) {
			try {
				while (resultSet.next()) {
					exportArray.add(getSearchResult(resultSet));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (exportArray.size() > 0) {
			Object[][] data = new Object[exportArray.size()][4];
			for (Object[] row : exportArray) {
				data[exportArray.indexOf(row)] = row;
			}
			initTableModel(data);
		}

	}

	private Object[] getSearchResult(ResultSet resultSet) {
		Object[] object = new Object[4];
		try {
			if (resultSet.getDate("date") != null) {
				object[0] = resultSet.getDate("date");
			}
			if (resultSet.getString("description") != null) {
				object[1] = resultSet.getString("description");
			}
			object[2] = resultSet.getDouble("value");
			if (resultSet.getString("name") != null) {
				object[3] = resultSet.getString("name");
			}

			return object;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private void exportInExcel() {
		ExcelExport export = new ExcelExport();
		export.openExcelFile("MONEYMANAGER_VORLAGE.xlsm",true);
		
		
		export.runExcelMakro("uebersicht.init");
		
		for(Object[] object: exportArray) {
			export.runExcelMakro("uebersicht.fuegeDatensatzEin", 
					(Date) object[0], ((String) object[3]) != null? (String) object[3]: "", (String) object[1],(double) object[2]);
		}
		
		//export.runExcelMakro("uebersicht.findeAlleKategorien");
	}

	@Override
	public String[] getEnableButtons() {
		return new String[] { "Suche", "Help" };
	}

	@Override
	public void updateView() {

	}

	private void initTableModel(Object[][] data) {
		DefaultTableModel tableModel = new DefaultTableModel(data, SPALTEN);
		table.setModel(tableModel);
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new MigLayout("", "[][grow][]", "[][][][][grow][]"));
			panel.add(getLblKategorie(), "cell 0 0,alignx trailing");
			panel.add(getComboBox(), "cell 1 0,growx");
			panel.add(getLblVon(), "cell 0 1,alignx trailing");
			panel.add(getTxtVon(), "cell 1 1,growx");
			panel.add(getLblBis(), "cell 0 2,alignx trailing");
			panel.add(getTxtBis(), "cell 1 2,growx");
			panel.add(getBtnSuche(), "cell 2 3");
			panel.add(new JScrollPane(getTable()), "cell 0 4 3 1,grow");
			panel.add(getBtnExport(), "cell 0 5");
		}
		return panel;
	}

	private JLabel getLblKategorie() {
		if (lblKategorie == null) {
			lblKategorie = new JLabel("Kategorie:");
		}
		return lblKategorie;
	}

	private JLabel getLblVon() {
		if (lblVon == null) {
			lblVon = new JLabel("von:");
		}
		return lblVon;
	}

	private JLabel getLblBis() {
		if (lblBis == null) {
			lblBis = new JLabel("bis:");
		}
		return lblBis;
	}

	private JTextField getTxtVon() {
		if (txtVon == null) {
			txtVon = new JTextField();
			txtVon.setColumns(10);
		}
		return txtVon;
	}

	private JTextField getTxtBis() {
		if (txtBis == null) {
			txtBis = new JTextField();
			txtBis.setColumns(10);
		}
		return txtBis;
	}

	private JTable getTable() {
		if (table == null) {
			table = new JTable();
		}
		return table;
	}

	private JButton getBtnExport() {
		if (btnExport == null) {
			btnExport = new JButton("Export");
		}
		return btnExport;
	}

	private JButton getBtnSuche() {
		if (btnSuche == null) {
			btnSuche = new JButton("Suche");
		}
		return btnSuche;
	}

	private JComboBox<CategoryImpl> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox<CategoryImpl>();
		}
		return comboBox;
	}
	
	
	private void exportInNumbers(){
		  // dateList, CategoryList, descriptionList, valueList
		  String[] dates = Arrays.copyOf(exportArray.get(0), exportArray.get(0).length, String[].class);
		  String[] categories = Arrays.copyOf(exportArray.get(1), exportArray.get(1).length, String[].class);
		  String[] descriptions = Arrays.copyOf(exportArray.get(3), exportArray.get(3).length, String[].class);
		  String[] values = Arrays.copyOf(exportArray.get(2), exportArray.get(2).length, String[].class);
		  new ExportToNumbers(dates, categories, descriptions, values);
		 }
}
