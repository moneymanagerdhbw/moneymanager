package view;

import model.DatabaseException;
import model.InvalidDataException;
import model.category.CategoryImpl;
import model.category.CategoryService;

import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.JColorChooser;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.JTextField;

import org.apache.log4j.Logger;

import controller.excelexport.ExcelExport;

public class CategoryDialog extends AbstractImplDialog<CategoryImpl> {
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);
	
	private JPanel pBackground;
	private JLabel lblName;
	private JLabel lblBeschreibung;
	private JTextField txtName;
	private JTextField txtBeschreibung;

	
	private JColorChooser jcc;
	
	
	private CategoryService categoryService;
	
	public CategoryDialog(int surrkey) throws DatabaseException {
		super("Kategorie");
		add(getPBackground(), BorderLayout.CENTER);
		initColorChooser();
		
		
		categoryService = new CategoryService();
		
		
		registerEvents();
		enableMaske(false);
		if(surrkey == -1) {
			actualImpl = new CategoryImpl();
		}else {
			actualImpl = categoryService.findOne(surrkey);
			fillMaske();
		}
	}
	
	private void initColorChooser() {
		jcc = new JColorChooser();
		pBackground.add(jcc, "cell 1 2, growx");
	}
	
	
	@Override
	public void enableMaske(boolean bearb) {
		txtName.setEnabled(bearb);
		txtBeschreibung.setEnabled(bearb);
		jcc.setEnabled(bearb);
	}
	
	


	@Override
	public void deleteImpl() {
		boolean deleted = categoryService.deleteImp(actualImpl);
		if(deleted) {
			super.closeThis(false);
			JOptionPane.showMessageDialog(this, "Erfolgreich gelöscht!");
		}else {
			JOptionPane.showMessageDialog(this, "Konnte nicht gelöscht werden!");
		}
	}

	@Override
	public void registerEvents() {
		super.registerEvents();
		txtBeschreibung.addFocusListener(initFocusListener());
		txtName.addFocusListener(initFocusListener());
	}

	@Override
	public void updateView() {
	}


	@Override
	public CategoryImpl storeValuesInImpl() {
		CategoryImpl impl = new CategoryImpl();
		Color color = jcc.getColor();
		impl.setColorB(color.getBlue());
		impl.setColorG(color.getGreen());
		impl.setColorR(color.getRed());
		impl.setName(txtName.getText());
		impl.setDescription(txtBeschreibung.getText());
		impl.setSurkey(actualImpl.getSurkey());
		return impl;
	}


	private void fillMaske() {
		txtName.setText(actualImpl.getName());
		txtBeschreibung.setText(actualImpl.getDescription());
		jcc.setColor(new Color(actualImpl.getColorR(), actualImpl.getColorG(), actualImpl.getColorB()));
	}
	
	
	private JPanel getPBackground() {
		if (pBackground == null) {
			pBackground = new JPanel();
			pBackground.setLayout(new MigLayout("", "[][grow]", "[][]"));
			pBackground.add(getLblName(), "cell 0 0,alignx trailing");
			pBackground.add(getTxtName(), "cell 1 0,growx");
			pBackground.add(getLabel_1(), "cell 0 1,alignx trailing");
			pBackground.add(getTextField_1(), "cell 1 1,growx");
		}
		return pBackground;
	}
	private JLabel getLblName() {
		if (lblName == null) {
			lblName = new JLabel("Name:");
		}
		return lblName;
	}
	private JLabel getLabel_1() {
		if (lblBeschreibung == null) {
			lblBeschreibung = new JLabel("Beschreibung:");
		}
		return lblBeschreibung;
	}
	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
			txtName.setColumns(10);
		}
		return txtName;
	}
	private JTextField getTextField_1() {
		if (txtBeschreibung == null) {
			txtBeschreibung = new JTextField();
			txtBeschreibung.setColumns(10);
		}
		return txtBeschreibung;
	}

	@Override
	public boolean saveMaske() {
		CategoryImpl impl = storeValuesInImpl();
		
		if(!actualImpl.isEqual(impl)) {
			try {
				impl.ueberpruefeDaten();
				
				categoryService.storeImplInDB(impl);
				
				JOptionPane.showMessageDialog(this, "Daten wurden gespeichert!");
				super.closeThis(false);
			} catch (InvalidDataException e) {
				JOptionPane.showMessageDialog(this, "Daten sind nicht valide!");
				LOGGER.error(e);
				return false;
			} catch (DatabaseException e) {
				JOptionPane.showMessageDialog(this, "Konnte nicht gespeichert werden");
				LOGGER.error(e);
				return false;
			}
		}
		return true;
	}


	
	
}
