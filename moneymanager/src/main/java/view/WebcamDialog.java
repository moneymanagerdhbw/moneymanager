package view;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import controller.webcam.WebcamCapture;
import marvin.gui.MarvinImagePanel;

import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;

public class WebcamDialog extends AbstractDialog{
	private JPanel pBackground;

	private MarvinImagePanel imagePanel;
	private JButton btnCapture;
	
	public WebcamDialog() {
		super("Aufnahme Webcam");
		add(getPBackground(), BorderLayout.CENTER);
		getPBackground().setLayout(new MigLayout("", "[410px][]", "[247px][]"));
		
		imagePanel = new MarvinImagePanel();
		pBackground.add(imagePanel, "cell 0 0,grow");
		try {
			WebcamCapture.showVideo(imagePanel);
			imagePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		registerEvents();
		pBackground.updateUI();
	}

	
	@Override
	public void registerEvents() {
		super.registerEvents();
		btnCapture.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String text = WebcamCapture.capture(imagePanel);
				try {
					WebcamCapture.stop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				new AnalysedTextFrame(text);
			}
		});
	}
	
	
	
	@Override
	public String[] getEnableButtons() {
		// TODO Auto-generated method stub
		return new String[] {"Hilfe"};
	}

	@Override
	public void updateView() {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void closeThis(boolean save)  {
		try {
			WebcamCapture.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.closeThis(save);
	}

	private JPanel getPBackground() {
		if (pBackground == null) {
			pBackground = new JPanel();
			pBackground.setBorder(new EmptyBorder(0, 20, 20, 20));
			pBackground.add(getBtnCapture(), "cell 1 1");
		}
		return pBackground;
	}
	private JButton getBtnCapture() {
		if (btnCapture == null) {
			btnCapture = new JButton("Aufnehmen");
		}
		return btnCapture;
	}
}
