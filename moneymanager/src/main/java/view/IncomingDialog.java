package view;

import javax.swing.JOptionPane;

import model.DatabaseException;
import model.InvalidDataException;
import model.category.CategoryImpl;
import model.category.CategoryService;
import model.incoming.IncomingImpl;
import model.incoming.IncomingService;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class IncomingDialog  extends AbstractImplDialog<IncomingImpl>{

	
	private IncomingService incomingService;
	private CategoryService categoryService;
	
	private JPanel pBackground;
	private JLabel lblDatum;
	private JLabel lblBeschreibung;
	private JLabel lblWert;
	private JLabel lblKategorie;
	private JTextField txtDatum;
	private JTextField txtBeschreibung;
	private JTextField txtWert;
	private JComboBox<CategoryImpl> comboBox;
	
	
	
	
	public IncomingDialog(int surrkey) {
		super("Einzahlung");
		add(getPBackground(), BorderLayout.CENTER);
		
		try {
			incomingService = new IncomingService();
			categoryService = new CategoryService();
			if(surrkey == -1) {
				actualImpl = new IncomingImpl();
				
			}else {
				actualImpl = incomingService.findOne(surrkey);
			}
		} catch (DatabaseException e) {
			e.printStackTrace();
		}
		
		fillMaske();
		registerEvents();
		enableMaske(false);
		
		
	}

	
	private void fillMaske() {
		CategoryImpl actualCatImpl = updateCategory();
		if(actualImpl != null) {
			comboBox.setSelectedItem(actualCatImpl);
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		txtBeschreibung.setText(actualImpl.getDescription());
		txtDatum.setText(actualImpl.getDate() != null?format.format(actualImpl.getDate()):"");
		txtWert.setText(actualImpl.getValue() +"");
	}


	private CategoryImpl updateCategory() {
		ResultSet resultSet = categoryService.find("Select * from category");
		CategoryImpl actualCatImpl = null;
		comboBox.removeAllItems();
		if (resultSet != null) {
			try {
				while(resultSet.next()) {
					CategoryImpl mImpl = categoryService.fillImpl(resultSet);
					if(mImpl.getSurkey() == actualImpl.getCategory()) {
						actualCatImpl = mImpl;
					}
					comboBox.addItem(mImpl);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return actualCatImpl;
	}
	
	@Override
	public void registerEvents() {
		super.registerEvents();
		txtBeschreibung.addFocusListener(initFocusListener());
		txtDatum.addFocusListener(initFocusListener());
		txtWert.addFocusListener(initFocusListener());
		comboBox.addFocusListener(initFocusListener());
	}
	
	@Override
	public boolean saveMaske() {
		IncomingImpl impl = storeValuesInImpl();
		if(!actualImpl.isEqual(impl)) {
			try {
				impl.ueberpruefeDaten();
				
				incomingService.storeImplInDB(impl);
				
				JOptionPane.showMessageDialog(this, "Speichern war erfolgreich");
				super.closeThis(false);
			} catch (InvalidDataException e) {
				JOptionPane.showMessageDialog(this, "Daten sind nicht valide!");
				e.printStackTrace();
			} catch (DatabaseException e) {
				JOptionPane.showMessageDialog(this, "Datenbankfehler");
				e.printStackTrace();
			}
			
		}
		return false;
	}

	@Override
	public IncomingImpl storeValuesInImpl() {
		IncomingImpl impl = new IncomingImpl();
		impl.setSurkey(actualImpl.getSurkey());
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		try {
			if(!txtDatum.getText().equals("")) {
				impl.setDate(format.parse(txtDatum.getText()));
			}else {
				impl.setDate(null);
			}
		} catch (ParseException e) {
			JOptionPane.showMessageDialog(this, "Datum hat mal ein Kack Format du Lusche");
		}
		impl.setDescription(txtBeschreibung.getText());
		impl.setCategory(((CategoryImpl) comboBox.getSelectedItem()).getSurkey()); 
		try{
			impl.setValue(Double.parseDouble(txtWert.getText()));	
		}catch(NumberFormatException e1) {
			JOptionPane.showMessageDialog(this, "Wert ist schrott");
		}
		return impl;
	}

	@Override
	public void deleteImpl() {
		boolean deleted = incomingService.deleteImp(actualImpl);
		if(deleted) {
			JOptionPane.showMessageDialog(this, "Daten wurden gelöscht");
			super.closeThis(false);
		}else {
			JOptionPane.showMessageDialog(this, "Löschen fehlgeschlagen");
		}
	}

	@Override
	public void updateView() {
		enableMaske(false);
		updateCategory();
	}

	@Override
	public void enableMaske(boolean bearb) {
		txtBeschreibung.setEnabled(bearb);
		txtDatum.setEnabled(bearb);
		txtWert.setEnabled(bearb);
		comboBox.setEnabled(bearb);
		
	}

	private JPanel getPBackground() {
		if (pBackground == null) {
			pBackground = new JPanel();
			pBackground.setLayout(new MigLayout("", "[][grow]", "[][][][]"));
			pBackground.add(getLblDatum(), "cell 0 0,alignx trailing");
			pBackground.add(getTxtDatum(), "cell 1 0,growx");
			pBackground.add(getLblBeschreibung(), "cell 0 1,alignx trailing");
			pBackground.add(getTxtBeschreibung(), "cell 1 1,growx");
			pBackground.add(getLblWert(), "cell 0 2,alignx trailing");
			pBackground.add(getTxtWert(), "cell 1 2,growx");
			pBackground.add(getLblKategorie(), "cell 0 3,alignx trailing");
			pBackground.add(getComboBox(), "cell 1 3,growx");
		}
		return pBackground;
	}
	private JLabel getLblDatum() {
		if (lblDatum == null) {
			lblDatum = new JLabel("Datum:");
		}
		return lblDatum;
	}
	private JLabel getLblBeschreibung() {
		if (lblBeschreibung == null) {
			lblBeschreibung = new JLabel("Beschreibung:");
		}
		return lblBeschreibung;
	}
	private JLabel getLblWert() {
		if (lblWert == null) {
			lblWert = new JLabel("Wert:");
		}
		return lblWert;
	}
	private JLabel getLblKategorie() {
		if (lblKategorie == null) {
			lblKategorie = new JLabel("Kategorie");
		}
		return lblKategorie;
	}
	private JTextField getTxtDatum() {
		if (txtDatum == null) {
			txtDatum = new JTextField();
			txtDatum.setColumns(10);
		}
		return txtDatum;
	}
	private JTextField getTxtBeschreibung() {
		if (txtBeschreibung == null) {
			txtBeschreibung = new JTextField();
			txtBeschreibung.setColumns(10);
		}
		return txtBeschreibung;
	}
	private JTextField getTxtWert() {
		if (txtWert == null) {
			txtWert = new JTextField();
			txtWert.setColumns(10);
		}
		return txtWert;
	}
	private JComboBox<CategoryImpl> getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox<CategoryImpl>();
		}
		return comboBox;
	}
}
