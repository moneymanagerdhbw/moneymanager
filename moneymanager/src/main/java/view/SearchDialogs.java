package view;

import javax.swing.table.DefaultTableModel;

public class SearchDialogs {
	
	
	public static final int SEARCH_INCOMING = 1;
	public static final int SEARCH_OUTGOING = 2;
	public static final int SEARCH_CATEGORY = 3;
	
	
	public static AbstractSearch getInstance(int instance) {
		switch(instance) {
		case SEARCH_INCOMING:
			return new SearchIncoming();
		case SEARCH_OUTGOING:
			return new SearchOutgoing();
		case SEARCH_CATEGORY:
			return new SearchCategory();
		}
		return null;
	}
	

	public static class SearchIncoming extends AbstractSearch {

		public SearchIncoming() {
			super("Datum", "Beschreibungs", "Betrag");
		}

		@Override
		public String getSearchSQL() {
			String where = "";
			if (!getTxtSuche1().getText().equals(""))
				where += "date = '" + getTxtSuche1().getText() + "' ";
			if (!getTxtSuche2().getText().equals(""))
				where += where.equals("") ? "description like '"
						+ getTxtSuche2().getText() + "' "
						: " and  description like '" + getTxtSuche2().getText()
								+ "' ";
			if (!getTxtSuche3().getText().equals(""))
				where += where.equals("") ? "value = "
						+ getTxtSuche2().getText() + " " : " and  value = "
						+ getTxtSuche2().getText() + " ";

			return "Select * from incoming "
					+ (where.equals("") ? "" : "where" + where);
		}

		@Override
		public DefaultTableModel getTableModel() {
			DefaultTableModel model = new DefaultTableModel(new Object[][] {},
					new String[] { "Descrpition", "Date", "Value" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};

			return model;
		}
	}

	public static class SearchOutgoing extends AbstractSearch {

		public SearchOutgoing() {
			super("Datum", "Beschreibungs", "Betrag");
		}

		@Override
		public String getSearchSQL() {
			String where = "";
			if (!getTxtSuche1().getText().equals(""))
				where += "date = '" + getTxtSuche1().getText() + "' ";
			if (!getTxtSuche2().getText().equals(""))
				where += where.equals("") ? "description = '"
						+ getTxtSuche2().getText() + "' "
						: " and  description like '" + getTxtSuche2().getText()
								+ "' ";
			if (!getTxtSuche3().getText().equals(""))
				where += where.equals("") ? "value = "
						+ getTxtSuche2().getText() + " " : " and  value = "
						+ getTxtSuche2().getText() + " ";

			return "Select * from outgoing "
					+ (where.equals("") ? "" : "where" + where);
		}

		@Override
		public DefaultTableModel getTableModel() {
			DefaultTableModel model = new DefaultTableModel(new Object[][] {},
					new String[] { "Descrpition", "Date", "Value" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};

			return model;
		}
	}

	public static class SearchCategory extends AbstractSearch {

		public SearchCategory() {
			super("Name", null, null);
		}

		@Override
		public String getSearchSQL() {
			String where = "";
			if (!getTxtSuche1().getText().equals(""))
				where += "name like '" + getTxtSuche1().getText() + "' ";
			
			
			return "Select * from category "
					+ (where.equals("") ? "" : "where" + where);
		}

		@Override
		public DefaultTableModel getTableModel() {
			DefaultTableModel model = new DefaultTableModel(new Object[][] {},
					new String[] { "Name", "Description" }) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			return model;
		}
	}

}
