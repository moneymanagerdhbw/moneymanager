package view;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

public abstract class AbstractDialog extends JPanel {
	private JPanel pClosePanel;
	private JLabel pLabelName;
	private JButton btnClose;
	
	
	
	
	
	public AbstractDialog(String name) {
		setLayout(new BorderLayout(0, 0));
		add(getPClosePanel(), BorderLayout.NORTH);
		pLabelName.setText(name);
	}
	
	
	
	
	public void registerEvents() {
		btnClose.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				closeThis(false);
				
			}
		});
		
		
	}
	
	public void closeThis() {
		((MainDialog) SwingUtilities.getWindowAncestor(this)).closeAbstractDialog(this);
	}
	
	public void closeThis(boolean save) {
		((MainDialog) SwingUtilities.getWindowAncestor(this)).closeAbstractDialog(this);
	}
	
	
	
	
	public abstract String[] getEnableButtons();
	public abstract void updateView();
	
	private JPanel getPClosePanel() {
		if (pClosePanel == null) {
			pClosePanel = new JPanel();
			pClosePanel.setBorder(new EmptyBorder(4, 4, 4, 4));
			pClosePanel.setLayout(new BorderLayout(0, 0));
			pClosePanel.add(getPLabelName(), BorderLayout.WEST);
			pClosePanel.add(getBtnClose(), BorderLayout.EAST);
		}
		return pClosePanel;
	}
	private JLabel getPLabelName() {
		if (pLabelName == null) {
			pLabelName = new JLabel("");
		}
		return pLabelName;
	}
	private JButton getBtnClose() {
		if (btnClose == null) {
			btnClose = new JButton("");
			btnClose.setIcon(new ImageIcon("images/close.png"));
		}
		return btnClose;
	}
}
