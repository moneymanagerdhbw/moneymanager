package controller.webcam;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import marvin.plugin.MarvinImagePlugin;
import marvin.plugin.MarvinPlugin;
import marvin.util.MarvinAttributes;
import marvin.util.MarvinPluginLoader;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import org.apache.log4j.Logger;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.FFmpegFrameRecorder;
import org.bytedeco.javacv.OpenCVFrameGrabber;
import org.eclipse.swt.graphics.Rectangle;

import controller.excelexport.ExcelExport;
import static org.bytedeco.javacpp.videoInputLib.*;

public class WebcamCapture {

	private static boolean running = true;
	private static boolean animationFinished = false;
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);

	
	private static MyGrabber grabber;
	
	
	public static String capture(MarvinImagePanel imgPanel) {
		String erg = "";
		try {
			if (grabber.running()) {
				IplImage img = grabber.grab();

				if (img != null) {
					BufferedImage bufimage = img.getBufferedImage();

					MarvinImage image = new MarvinImage(bufimage);
					
					System.out.println(bufimage.getWidth());
					System.out.println(bufimage.getHeight());
					File outFile = new File("saved.png");
					BufferedImage mImage = bufimage.getSubimage(20, 80, 580, 300);
					erg =  ocrImagefile(mImage);
					ImageIO.write(bufimage.getSubimage(20, 80, 580, 300), "png", outFile);
					imgPanel.setImage(image);
					grabber.stop();
					System.out.println("finished");
				}
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return erg;
	}
	
	
	public static String ocrImagefile(BufferedImage bufferedImage) throws TesseractException{
		Tesseract instance = Tesseract.getInstance();
		return instance.doOCR(bufferedImage);
	}

	public static class MyGrabber extends OpenCVFrameGrabber {

		private volatile boolean isRunning = false;

		public MyGrabber(int device) {
			super(device);
		}

		@Override
		public void start() throws Exception {
			super.start();
			isRunning = true;
		}

		@Override
		public void stop() throws Exception {
			isRunning = false;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				LOGGER.error(e);
			}
			super.stop();

		}

		public synchronized boolean running() {
			return isRunning;
		}

	}
	
	
	public static void stop() throws Exception {
		grabber.stop();
	}

	public static void showVideo(final MarvinImagePanel panel) throws Exception {

		grabber = new MyGrabber(0);

		
		grabber.start();

		final Thread t = new Thread(new Runnable() {

			public void run() {
				try {
					grabber.start();
					IplImage img = grabber.grab();
					BufferedImage bufImage = img.getBufferedImage();
					MarvinImage image = new MarvinImage(bufImage);

					while (running && grabber.running()) {
						img = grabber.grab();
						bufImage = img.getBufferedImage();
						 Graphics2D graphics = bufImage.createGraphics();
						 graphics.setColor(Color.WHITE);
						 graphics.drawRect(20, 80, 580, 300);
						 graphics.dispose();
						 image = new MarvinImage(bufImage.getWidth(), bufImage.getHeight());
						
						 
						 image.setBufferedImage(bufImage);
						 
						 panel.setImage(image);
						Thread.sleep(30);
//						System.out.println("runs");
					}
					animationFinished = true;

				} catch (Exception e) {
					LOGGER.error(e);
				}

			}
		});
		t.start();

	}


}
