package controller.numbersexport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.log4j.Logger;

import controller.excelexport.ExcelExport;

public class ExportToNumbers {
	String[] keyAry = { "key_date", "key_category", "key_value", "key_description"};
	String[] dateList;
	String[] categoryList;
	String[] descriptionList;
	String[] valueList;
	public String[][] columns;
	String[] allCategories;
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);
	
	public ExportToNumbers(String[] dateList, String[] categoryList, String[] descriptionList, String[] valueList) {
		this.dateList = dateList;
		this.categoryList = categoryList;
		this.valueList = valueList;
		this.descriptionList = descriptionList;
		this.columns = new String[][] {dateList, categoryList, descriptionList,valueList };
	}
	
	public void getAllCategoryNames(String[] categoryList){
		List<String> categories = new ArrayList<String>();
		for (int i = 0; i < categoryList.length; i++) {
			if (!categories.contains(categoryList[i])){
				categories.add(categoryList[i]);
			}
		}
		
		String[] ary = new String[categories.size()];
		allCategories = categories.toArray(ary);
	}


	public StringBuffer openScript() {
		StringBuffer script = new StringBuffer();
		try {
			BufferedReader in = new BufferedReader(new FileReader(
					"fillOutTemplateForNumbers.txt"));
			String zeile = null;
			while ((zeile = in.readLine()) != null) {
				script.append(zeile + "\n");
			}
		} catch (IOException e) {
			LOGGER.error(e);
		}
		return script;
	}

	public StringBuffer replacePlaceholder(StringBuffer script) {
		StringBuffer scriptWithOutPlaceholder = script;
		
		getAllCategoryNames(categoryList);
		String keyAllCat = "key_allCat";
		int pos;
		for (int i = 0; i < allCategories.length; i++) {
			pos = script.indexOf(keyAllCat);
			if (pos < 0)
				break;
			if (i != allCategories.length - 1) {
				script.replace(pos, pos + keyAllCat.length(), "\"" + allCategories[i]
						+ "\", " + keyAllCat);
			} else
				script.replace(pos, pos + keyAllCat.length(), "\"" + allCategories[i]
						+ "\"");
		}
		
		String key = "key_date";
		int k = 0;
		for (String[] column : columns) {
			key = keyAry[k];
			for (int i = 0; i < column.length; i++) {
				pos = script.indexOf(key);
				if (pos < 0)
					break;
				if (i != dateList.length - 1) {
					script.replace(pos, pos + key.length(), "\"" + column[i]
							+ "\", " + key);
				} else
					script.replace(pos, pos + key.length(), "\"" + column[i]
							+ "\"");
			}
			k++;
		}
		return scriptWithOutPlaceholder;
	}

	public String[] getDateList() {
		return dateList;
	}

	public void setDateList(String[] dateList) {
		this.dateList = dateList;
	}

	public String[] getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(String[] categoryList) {
		this.categoryList = categoryList;
	}

	public String[] getDescriptionList() {
		return descriptionList;
	}

	public void setDescriptionList(String[] descriptionList) {
		this.descriptionList = descriptionList;
	}

	public String[] getValueList() {
		return valueList;
	}

	public void setValueList(String[] valueList) {
		this.valueList = valueList;
	}

	public String[][] getColumns() {
		return columns;
	}

	public void setColumns(String[][] columns) {
		this.columns = columns;
	}

	public String[] getAllCategories() {
		return allCategories;
	}

	public void setAllCategories(String[] allCategories) {
		this.allCategories = allCategories;
	}

	public void executeIt(String script) {
		File file = new File("MONEYMANAGER_VORLAGE_NUMBERS.numbers");
		String filepath = file.getAbsolutePath();
		script = script.replace("key_path", filepath);
		
		//System.out.println(script);
		
		ScriptEngineManager mgr = new ScriptEngineManager();
		ScriptEngine engine = mgr.getEngineByName("AppleScriptEngine");
		try {
			engine.eval(script);
		} catch (ScriptException e) {
			LOGGER.error(e);
		}
	}
	
	public static void main(String[] args) {
		File file = new File("MONEYMANAGER_VORLAGE_NUMBERS.numbers");
		System.out.println("absolut path: "+file.getAbsolutePath());;
		
	}
}