package controller.excelexport;

import java.io.File;

import org.apache.log4j.Logger;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;

public class ExcelExport {

	
	private ActiveXComponent myExcel;
	private Dispatch workbooks;
	private File file;
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);
	
	
	public ActiveXComponent openExcelFile(String filename, boolean visible) {
		try {
			myExcel = new ActiveXComponent("Excel.Application");
		}catch(Exception e) {
			LOGGER.error(e);
		}
		workbooks = myExcel.getProperty("Workbooks").toDispatch();
		myExcel.setProperty("visible", visible);
		try {
			file = new File("MONEYMANAGER_VORLAGE.xlsm");
			Variant dispatch = Dispatch.call(workbooks, "Open", file.getAbsolutePath());
		}catch(Exception e) {
			LOGGER.error(e);
		}
		return myExcel;
	}

	
	public void init () {
		Dispatch.call(myExcel, "uebersicht.init");
	}



	public void runExcelMakro(String name, Object... parameters) {
		switch(parameters.length) {
		case 0:
			Dispatch.call(myExcel,"Run",name);
			break;
		case 1:
			Dispatch.call(myExcel,"Run",name, parameters[0]);
			break;
		case 2:
			Dispatch.call(myExcel,"Run",name, parameters[0], parameters[1]);
			break;
		case 3:
			Dispatch.call(myExcel,"Run",name, parameters[0], parameters[1], parameters[2]);
			break;
		case 4:
			Dispatch.call(myExcel,"Run",name, parameters[0], parameters[1], parameters[2], parameters[3]);
			break;
		default:
			break;
		}
	}
		
	}