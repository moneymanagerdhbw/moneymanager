package model.incoming;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.log4j.Logger;

import controller.excelexport.ExcelExport;
import model.DatabaseException;
import model.DatabaseService;

public class IncomingService extends DatabaseService<IncomingImpl>{

	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);
	
	/**
	 * NUR F�R TESTEN 
	 * @throws DatabaseException
	 */
	public IncomingService() throws DatabaseException {
		super();
	}

	@Override
	public IncomingImpl fillImpl(ResultSet resultSet) {
		IncomingImpl impl = new IncomingImpl();
		try {
			impl.setCategory(resultSet.getInt("category"));
			impl.setDate(new Date(resultSet.getDate("date").getTime()));
			impl.setDescription(resultSet.getString("description"));
			impl.setSurkey(resultSet.getInt("id"));
			impl.setValue(resultSet.getDouble("value"));
		}catch(SQLException ex) {
			LOGGER.error(ex);
		}
		return impl;
	}

	
	public IncomingImpl findOne(int id) throws DatabaseException{
		String sql = "select * from incoming where id = " + id;
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					return fillImpl(resultSet);
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
		return null;
	}
	
}
