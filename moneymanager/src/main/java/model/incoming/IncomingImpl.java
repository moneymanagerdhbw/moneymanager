package model.incoming;

import java.util.Date;

import model.DataAbstractImpl;
import model.InvalidDataException;

public class IncomingImpl  extends DataAbstractImpl<IncomingImpl>{

	
	private int category = 0;
	private String description = "";
	private Date date;
	private double value;
	
	
	
	public IncomingImpl() {
		super("incoming", "id");
	}

	
	
	
	@Override
	public boolean isEqual(IncomingImpl abstractImpl) {
		if(!description.equals(abstractImpl.getDescription())) return false;
		if(category != abstractImpl.getCategory()) return false;
		if(abstractImpl.getDate() != null && !date.equals(abstractImpl.getDate()) ) return false;
		if(value != abstractImpl.getValue())	return false;
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws InvalidDataException {
		if(value < 0) throw new InvalidDataException("Betrag", "Darf nicht kleiner 0 sein");
		return true;
	}
	
	
	
	
	
	
	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}




	@Override
	public Object[] getSearchValue() {
		return new Object[] {description, date, value};
	}

	

}
