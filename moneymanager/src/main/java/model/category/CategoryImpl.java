package model.category;
import model.DataAbstractImpl;
import model.InvalidDataException;


public class CategoryImpl extends DataAbstractImpl<CategoryImpl> {

	
	private String name = "";
	private int colorR = 255;
	private int colorG = 255;
	private int colorB = 255;
	private String description = "";
	
	public CategoryImpl() {
		super("category", "id");
		
	}

	@Override
	public boolean isEqual(CategoryImpl abstractImpl) {
		if(!name.equals(abstractImpl.getName())) return false;
		if(colorR != abstractImpl.getColorR()) return false;
		if(colorG != abstractImpl.getColorG()) return false;
		if(colorB != abstractImpl.getColorB()) return false;
		if(!description.equals(abstractImpl.getDescription())) return false;
		return true;
	}

	@Override
	public boolean ueberpruefeDaten() throws InvalidDataException {
		if(colorR > 255 || colorR < 0) throw new InvalidDataException("ColorR", "Falsch");
		if(colorR > 255 || colorR < 0) throw new InvalidDataException("ColorG", "Falsch");
		if(colorR > 255 || colorR < 0) throw new InvalidDataException("ColorB", "Falsch");
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getColorR() {
		return colorR;
	}

	public void setColorR(int colorR) {
		this.colorR = colorR;
	}

	public int getColorG() {
		return colorG;
	}

	public void setColorG(int colorG) {
		this.colorG = colorG;
	}

	public int getColorB() {
		return colorB;
	}

	public void setColorB(int colorB) {
		this.colorB = colorB;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public Object[] getSearchValue() {
		return new Object[] {name, description};
	}

	@Override
	public String toString() {
		return name;
	}
}
