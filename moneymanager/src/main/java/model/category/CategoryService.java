package model.category;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import controller.excelexport.ExcelExport;
import model.DatabaseException;
import model.DatabaseService;
import model.outgoing.OutgoingImpl;

public class CategoryService  extends DatabaseService<CategoryImpl>{
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);

	public CategoryService()
			throws DatabaseException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public CategoryImpl fillImpl(ResultSet resultSet) {
		CategoryImpl impl = new CategoryImpl();
		try {
			impl.setSurkey(resultSet.getInt("id"));
			impl.setColorB(resultSet.getInt("colorB"));
			impl.setColorR(resultSet.getInt("colorR"));
			impl.setColorG(resultSet.getInt("colorG"));
			impl.setDescription(resultSet.getString("description"));
			impl.setName(resultSet.getString("name"));
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		return impl;
	}

	
	
	public CategoryImpl findOne(int id) throws DatabaseException{
		String sql = "select * from category where id = " + id;
		ResultSet resultSet = find(sql);
		if(resultSet != null) {
			try {
				while(resultSet.next()) {
					return fillImpl(resultSet);
				}
			} catch (SQLException e) {
				LOGGER.error(e);
			}
		}
		return null;
	}
}