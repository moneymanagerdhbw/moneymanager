package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import controller.excelexport.ExcelExport;


public abstract class DatabaseService <T extends DataAbstractImpl>{
	private Connection connection = null;
	private String CONNECTION_STRING;
	private String USER;
	private String PASSWORD;
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);
	
	protected DatabaseService(String userName, String password) {
		CONNECTION_STRING = "jdbc:mysql://localhost/db_"+userName;
		USER = userName;
		PASSWORD = password;
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			LOGGER.error(ex);
		}
		
		try {
			connection = DriverManager.getConnection(CONNECTION_STRING, USER, PASSWORD);
		} catch(SQLException ex){
			LOGGER.error(ex);
		}
	}
	
	public static void initDatabaseService(String username, String hashPW) {
		
	}
	
	
	
	
	/**
	 *  Dies ist der KOnstruktor zum testen der Anwendung, wenn noch keine User angelegt wurden
	 */
	protected DatabaseService()  {
		CONNECTION_STRING = "jdbc:mysql://localhost/db_username_template";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {
			LOGGER.error(ex);
		}
		
		try {
			// USER MUSS ANGELEGT WERDEN IN DER LOKALEN DATENBANK
			connection = DriverManager.getConnection(CONNECTION_STRING, "root", "");
		} catch(SQLException ex){
			LOGGER.error(ex);
		}
		
	}
	
	
	

	private int generateNewSurkey(String tableName, String surkeyField) {
		String sql = "select max(" +surkeyField+") from " + tableName;
		ResultSet resultSet = find(sql);
		int neuerSurrkey = 0;
				
		try {
			while (resultSet.next()) {
				neuerSurrkey = resultSet.getInt("max("+surkeyField+")");
			}
		} catch (SQLException e) {
			LOGGER.error(e);
		}
		neuerSurrkey = neuerSurrkey +1;
		return neuerSurrkey;
	}
	
	protected boolean excecute(String sql) {
		try {
			Statement sqlStatement = connection.createStatement();
			sqlStatement.execute(sql);
			return true;  
		} catch (SQLException e) {
			LOGGER.error(e);
			return false;
		}
	}
	
	
	public boolean hasConnection() {
		if(connection == null) return false;
		else return true;
	}

	public ResultSet find(String sql) {
		try {
			Statement sqlStatement = connection.createStatement();
			return sqlStatement.executeQuery(sql);
			
		}catch(SQLException e) {
			LOGGER.error(e);
			return null;
		}
	}
	
	public void storeImplInDB(T abstractImpl) throws DatabaseException, InvalidDataException{
		try {
			abstractImpl.ueberpruefeDaten();
			String sql = generateInsertOrUpdateStatment(abstractImpl);
			excecute(sql);
		}catch(SQLException e) {
			LOGGER.error(e);
		}
	}
	
	
	private String generateInsertOrUpdateStatment(T abstractImpl) throws SQLException  {
		if(abstractImpl.getSurkey() == -1) {
			int newSurkey = generateNewSurkey(abstractImpl.getTableName(), abstractImpl.getSurkeyField());
			abstractImpl.setSurkey(newSurkey);
		
			HashMap<String, Object > dataset = abstractImpl.generateDatabaseHashSet();
			return generateInsertStatement(dataset, abstractImpl.getTableName());
		}else {
			HashMap<String, Object> dataset = abstractImpl.generateDatabaseHashSet();
			return generateUpdateStatement(dataset, abstractImpl.getTableName(), abstractImpl.getSurkeyField());
		}
	}
	
	private String generateUpdateStatement(HashMap<String, Object> dataset,
			String tableName, String surrkeyField) {
		String sql = "update " + tableName + " set ";
		String values = "";
		for(String key: dataset.keySet()) {
			Object ob = dataset.get(key);
			if(ob.getClass().equals(String.class)) {
				values += ", " + key + "=\"" + (String) ob + "\"";
			}else {
				if(ob.getClass().equals(Date.class)) {
					values += ",   "+ key+"= '" + new java.sql.Date(((Date) ob).getTime()) + "'";
				}else {
					values += ", " + key +"="+ ob.toString();
				}
			}
		}
		values = values.substring(1);
		sql += values + " where " + surrkeyField + "=" +dataset.get(surrkeyField);
		return sql;
	}

	private String generateInsertStatement(HashMap<String, Object> dataset,
			String tableName) {
		String sql = "INSERT INTO " + tableName +" (";
		String columns = "";
		String values = ""; 
		for(String key: dataset.keySet()) {		
			columns += ", " + key ;
			Object ob = dataset.get(key);
			if(ob.getClass().equals(String.class)) {
				values += ", \"" + (String) ob + "\"";
			}else {
				if(ob.getClass().equals(Date.class)) {
					values += ", '" + new java.sql.Date(((Date) ob).getTime()) + "'";
				}else {
					values += ", " +  ob.toString();
				}
				
			}
			
		}
		columns = columns.substring(1);
		values = values.substring(1);
		sql +=columns + ") values(" + values + ")";
		return sql;
	}
	
	public boolean deleteImp(DataAbstractImpl abstractImpl) {
		String sql = "Delete from " + abstractImpl.getTableName() + 
					" where " + abstractImpl.getSurkeyField() + "=" + abstractImpl.getSurkey();
		return excecute(sql);	
	}

	public abstract T fillImpl(ResultSet resultSet);
	 	
}
