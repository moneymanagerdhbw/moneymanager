package model;

public class InvalidDataException extends Exception{

	String msg ="";
	
	
	
	
	public InvalidDataException(String feld, String value) {
		msg = feld +  " ist mit dem Wert: " + value + " nicht valide";
	}
	
	public String toString() {
		return msg;
	}
}
