package model;

import java.lang.reflect.Field;
import java.util.HashMap;

import org.apache.log4j.Logger;

import controller.excelexport.ExcelExport;



public abstract class DataAbstractImpl <T extends DataAbstractImpl<?>> {
	private int surkey =-1;
	private String tableName;
	private String surkeyField;
	private static final Logger LOGGER = Logger.getLogger(ExcelExport.class);
	
	protected DataAbstractImpl(String tableName, String surrkeyField) {
		this.tableName = tableName;
		this.surkeyField = surrkeyField;
	}

	public String getSurkeyField() {
		return surkeyField;
	}

	public void setSurkeyField(String surkeyField) {
		this.surkeyField = surkeyField;
	}

	public int getSurkey() {
		return surkey;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setSurkey(int surkey) {
		this.surkey = surkey;
	}
	
	
	
	public abstract boolean isEqual(T abstractImpl);
	public abstract boolean ueberpruefeDaten() throws InvalidDataException;
	public abstract Object[] getSearchValue();
	
	
	
	protected  HashMap<String, Object> generateDatabaseHashSet() {
		HashMap<String, Object> data = new HashMap<String, Object>();
		Field[] fields = this.getClass().getDeclaredFields();
		data.put(surkeyField, surkey);
		for(Field field:fields) {
			try {
				field.setAccessible(true);
				data.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException e) {
				LOGGER.error(e);
			} catch (IllegalAccessException e) {
				LOGGER.error(e);
			}
		}
		return data;
	};
	
}
