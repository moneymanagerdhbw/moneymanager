package exportexcel;

import static org.junit.Assert.*;

import java.sql.Date;

import org.junit.Test;

import controller.excelexport.ExcelExport;

public class ExcelExportTest {

	@Test
	public void test() {
		ExcelExport export = new ExcelExport();
		export.openExcelFile("MONEYMANAGER_VORLAGE.xlsm",true);
		
		
		export.runExcelMakro("uebersicht.init");
		
		for(int i = 0; i < 20; i++) {
			export.runExcelMakro("uebersicht.fuegeDatensatzEin", 
					new Date(System.currentTimeMillis()).toString(), "Tanken", "HIer habe ich getankt", -20.50);
			export.runExcelMakro("uebersicht.fuegeDatensatzEin", 
					new Date(System.currentTimeMillis()).toString(), "Lohn", "Lohn bekommen", 200);
			
		}
		//export.runExcelMakro("uebersicht.findeAlleKategorien");
	}

}
