package datenbanktest;

import static org.junit.Assert.*;

import java.util.Date;

import model.DatabaseException;
import model.InvalidDataException;
import model.category.CategoryImpl;
import model.category.CategoryService;
import model.outgoing.OutgoingImpl;
import model.outgoing.OutgoingService;

import org.junit.Test;

public class CategoryTest {

	@Test
	public void test() {
		try {
			CategoryService service = new CategoryService();
			CategoryImpl impl = new CategoryImpl();
			impl.setName("TestCat");
			impl.setDescription("Dies ist eine Testeinnahme");
			impl.setColorR(10);
			impl.setColorG(10);
			impl.setColorB(10);
			
			service.storeImplInDB(impl);
			
			CategoryImpl impl2 = service.findOne(impl.getSurkey());
			assertNotEquals(impl2, null);
			
			impl.setDescription("Dies ist eine Testkat");
			impl.setColorR(20);
			impl.setColorG(20);
			impl.setColorB(20);
			assertEquals(impl2.isEqual(impl), false);
			
			service.storeImplInDB(impl2);
			impl = service.findOne(impl2.getSurkey());
			
			assertEquals(impl.isEqual(impl2), true);
			
			service.deleteImp(impl);
			
			assertEquals(service.findOne(impl.getSurkey()), null);
			
			
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Datenbankfehler");
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
