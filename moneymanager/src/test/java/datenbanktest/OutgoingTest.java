package datenbanktest;

import static org.junit.Assert.*;

import java.util.Date;

import model.DatabaseException;
import model.InvalidDataException;
import model.incoming.IncomingImpl;
import model.incoming.IncomingService;
import model.outgoing.OutgoingImpl;
import model.outgoing.OutgoingService;

import org.junit.Test;

public class OutgoingTest {

	@Test
	public void test() {
		try {
			OutgoingService service = new OutgoingService();
			OutgoingImpl impl = new OutgoingImpl();
			impl.setCategory(1);
			impl.setDate(new Date(System.currentTimeMillis()));
			impl.setDescription("Dies ist eine Testeinnahme");
			impl.setValue(-455.50);
			
			service.storeImplInDB(impl);
			
			OutgoingImpl impl2 = service.findOne(impl.getSurkey());
			assertNotEquals(impl2, null);
			
			impl2.setCategory(2);
			impl2.setValue(-210.00);
			assertEquals(impl2.isEqual(impl), false);
			
			service.storeImplInDB(impl2);
			impl = service.findOne(impl2.getSurkey());
			
			assertEquals(impl.isEqual(impl2), true);
			
			service.deleteImp(impl);
			
			assertEquals(service.findOne(impl.getSurkey()), null);
			
			
			
		} catch (DatabaseException e) {
			e.printStackTrace();
			fail("Datenbankfehler");
		} catch (InvalidDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
