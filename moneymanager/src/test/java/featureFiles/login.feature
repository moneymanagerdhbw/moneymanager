Feature: Login

Scenario: new User
	Given the user has no account
	And he is on the Login Screen
	When the user selects „register“
	And fills out the following form
	Then the program creates a new User in the Usertable
	And the program creates a new user specific database

Scenario: existing User logs in with correct data
	Given the user has an account
	And he is not logged in
	And he is on the Login Screen
	When the user fills out the form
	And the data is valid then he is logged in