Feature: Add Data
	Data should only be added by logged in users
	Data should only be saved if they are valid
	Logged in user can not add Data of an other user

Scenario: Add new Data to existing category
	Given an empty form
	And multiply categories
	When I select an existing category
	And fill out the form with valid data
	And there is no database error
	Then the data should be saved
	
Scenario: Add an unexisting category
	Given an empty form
	When I select „Create Category“
	Then I can give a name to the category
	And the category will be saved