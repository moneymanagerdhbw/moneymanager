Feature: analyse bank statement

Scenario: analyse bank statement
	Given user selected a template of the type of bank statement
	And user has an opportunity to save the bank statement in a file
	And the user loads the file in the program
	And the program formats the file
	When the program can analyze the file
	Then it focus the data
	And the useless Add Data starts