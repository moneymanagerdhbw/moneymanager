package controller.numbersExport;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.numbersexport.ExportToNumbers;

public class ExportToNumbersTest {
	String[] dateTestList = { "14.11.2014", "17.11.2014", "19.11.2014",
			"04.12.2014" };
	String[] categoryTestList = { "Auto", "Essen", "Unimaterialien", "Agostea",
			"Agostea", "Auto", "Unimaterialien", "Schule" };

	String[] descriptionTestList = { "Tanken bei Aral", "Yufka essen",
			"Schulblock und Kulis gekauft", "Vollsuff slebe :-)" };
	String[] valueTestList = { "50", "4,75", "3", "61,7" };
	String[][] columns = new String [][] {dateTestList, categoryTestList, descriptionTestList, valueTestList};

	@Test
	public void test() {
	
		// CONSTRUCTOR TESTING
		ExportToNumbers ex = new ExportToNumbers(dateTestList, categoryTestList, descriptionTestList, valueTestList);
		
		for (int i = 0; i < dateTestList.length; i++) {
			assertEquals(dateTestList[i], ex.getDateList()[i]);
			assertEquals(categoryTestList[i], ex.getCategoryList()[i]);
			assertEquals(valueTestList[i], ex.getValueList()[i]);
			assertEquals(descriptionTestList[i], ex.getDescriptionList()[i]);
		}
		
		for (int i = 0; i < ex.columns.length; i++) {
			for (int j = 0; j < ex.columns[i].length; j++) {
				assertEquals(columns[i][j], ex.columns[i][j]);
			}			
		}
		
		// GETALLCATEGORYNAMES TESTING
		ex.getAllCategoryNames(categoryTestList);
		String[] testAry = { "Auto", "Essen", "Unimaterialien", "Agostea", "Schule" };
		
		for (int i = 0; i < testAry.length; i++) {
			assertEquals(testAry[i], ex.getAllCategories()[i]);
		}

		//
	//	StringBuffer script = ex.replacePlaceholder(new StringBuffer(ex.openScript().toString()));
		//System.out.println(script);
		
	//	ex.executeIt(script.toString());
		ex.executeIt(ex.replacePlaceholder(ex.openScript()).toString());
	}
}